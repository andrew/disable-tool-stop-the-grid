#!/usr/bin/python3
"""
Copyright 2023 Andrew Bogott
Copyright 2023 Wikimedia Foundation, Inc.

This script is free software, and comes with ABSOLUTELY NO WARRANTY.
It may be used, redistributed and/or modified under the terms of the GNU
General Public Licence (see http://www.fsf.org/licensing/licenses/gpl.txt).
"""

import argparse
import disable_tool

argparser = argparse.ArgumentParser(
    "stop_grid_for_tool",
    description="Stop grid jobs and archive crontab for a given tool. Must be run on the toolforge crontab node.",
)

argparser.add_argument(
    "--enable",
    help="reverse the action of a previous run. Restores crontab but does not start services.",
    action="store_true",
)

argparser.add_argument("tool")
args = argparser.parse_args()

if args.enable:
    disable_tool.unarchive_cron(args.tool)
else:
    disable_tool.archive_cron(args.tool)
    disable_tool.kill_grid_jobs(args.tool)
