#!/usr/bin/python3
"""
Copyright 2023 Andrew Bogott
Copyright 2023 Wikimedia Foundation, Inc.

This script is free software, and comes with ABSOLUTELY NO WARRANTY.
It may be used, redistributed and/or modified under the terms of the GNU
General Public Licence (see http://www.fsf.org/licensing/licenses/gpl.txt).
"""

import argparse
import disable_tool
import os

DISABLED_MESSAGE = """Your tool has been disabled as part of the grid-engine
shutdown timeline. We have tried to contact you about this timeline but
have so far been able to reach the tool owners.

If you need to temporarily re-enable this tool on the grid, begin by
locating the task associated with your tool (it will be a subtask of
https://phabricator.wikimedia.org/project/view/6135/) and comment on
that task. Please include your plan for migration as well as a request
to be re-enabled for a limited time.

You may also get speedier support with re-enabling by using the 'help' keyword
on the IRC wikimedia-cloud channel. Please engage on phabricator before
pinging admins on IRC.

Details about this process can be found here:

https://wikitech.wikimedia.org/wiki/News/Toolforge_Grid_Engine_deprecation#Timeline
"""


argparser = argparse.ArgumentParser(
    "disable_grid_for_tool",
    description="Disable the grid engine for a given tool. Must be run on the grid master.",
)

argparser.add_argument(
    "--enable",
    help="reverse the action of a previous run. Enables the grid but does not start services.",
    action="store_true",
)

argparser.add_argument("tool")
args = argparser.parse_args()

disabled_file_path = os.path.join(
    disable_tool.tool_dir(args.tool), "TOOL_DISABLED"
)
with open(disabled_file_path, "w") as disabled_file:
    disabled_file.write(DISABLED_MESSAGE)

if args.enable:
    disable_tool.delete_grid_quota(args.tool)
else:
    disable_tool.create_grid_quota(args.tool)
